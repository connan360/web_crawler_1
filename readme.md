
# Web Crawler

Your objective is to write a simple web crawler in a JVM based programming (preferably in Java, Scala or Kotlin).

The input to the crawler should take a website such as https://babylonhealth.com. The crawler should follow links that are internal to the domain but NOT links to external websites such as the NHS or social media (i.e. LinkedIn, Facebook, Instagram etc). Given the input of the URL the crawler should output a sitemap which shows the pages URLs and links to other pages.


## Tradeoff
* Doesn't take into account if there is an update to a webpage once it's crawled and parsed. (i.e only one visit per unique url)
* Only html type files are crawled & parsed
* Doesn't implement robot exclusion protocol
* Doesn't take into account infinite redirect loops
* Implements as a console application
* 

## Design
* Two queues:
    * ParsingQueue: Used by paser service which picks pages from the queue to be parsed. Once parsed, new links are added to ProcessingQueue.
    * ProcessingQueue: Used by crawler service which picks links from the queue to send a get request. Page retrieved is added to ParsingQueue
* HashMap:
    * Used for storing list of pages crawled.      

## Features
* Prints out sitemap
* Console Application
* In-memory datastore
* Stand-alone jar (no need for a pre-installed container/server)

## Requirements
* Java 8
* Maven

## Libraries
* jsoup
* mockito
* junit

## How to build the application
Checkout the project from this repository, then run
```
    mvn clean package
```
## How to run tests
* Checkout the project from this repository, then run
```
    mvn clean verify
```
## How to run the application
Build the application then run from target folder
```
    java -jar crawler-0.0.1-SNAPSHOT.jar
```

## How to use the application
* Enter the URL from the command line
* The process will print out list of parent and it's asssociated child urls
* type ctrl + c to terminate the crawling
